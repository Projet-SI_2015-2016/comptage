from flask import Flask, render_template, url_for
app = Flask(__name__)


@app.route('/')
def hello(name=None):
    return render_template('html.html', name=name)

@app.route("/lol")
def about():
    return 'lol'

if __name__ == "__main__":
    app.run(debug=True)
