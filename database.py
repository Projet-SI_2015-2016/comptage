import sqlite3


class Database():
    def __init__(self, dbfile):
        self.dbfile = dbfile
        self.conn = None
        self.c = None

    def connect_to_db(self):
        self.conn = sqlite3.connect(self.dbfile)
        self.c = self.conn.cursor()

    def create_table(self, table_columns):
        self.c.execute("CREATE TABLE ? (date, direction)",
                       table, columns)

    def insert_content(self, table, content):
        self.execute("INSERT INTO ? VALUES ?", table, content)

    def remove_content(self, table_name, column_name):
        self.execute("DELETE FROM ? WHERE column = ?", table_name, column_name)

    def get_all_content(self, table_name):
        content = []
        try:
            cur = self.execute("SELECT * FROM ?", table_name)
            for r in cur:
                content.append(table_name(r[0]))
        except sqlite3.OperationalError:
            print("La table \'{}' n\'existe pas.".format(table_name))
        return content

    def disconnect_from_db(self):
        self.conn.commit()
        self.conn.close()

