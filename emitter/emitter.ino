/* Send via Zigbee if someone enter or exit the room */
/* Pin 2: IR receiver for outside to inside */
/* Pin 3: IR receiver for inside to outside */

int lastStateIn = 0;
int lastStateOut = 0;

int
fullCrossing (int firstCaptorPin,
        int secondCaptorPin,
        unsigned int _delay)
{
  unsigned int p = 0;
  
  while (p < _delay)
    {
      if (digitalRead (firstCaptorPin) == HIGH)
      {
        if (digitalRead (secondCaptorPin) == HIGH)
        {
          // Full Crossed
          return 1;
        }
      }
      ++p;
      delay(1);
    }
  // Not Full Crossed
    return 0;
}

void
setup() 
{
 pinMode(2,INPUT);
 pinMode(3,INPUT);
 Serial.begin(9600);
}

void
loop()
{
  if (digitalRead (2) == HIGH)
  {
    if (lastStateIn == 0 && lastStateOut == 0)
    {
      if (fullCrossing (2, 3, 1000))
      {
        Serial.print ("1");
        delay(250);
      }
    }
    lastStateIn = 1;
    lastStateOut = 0;
  }
  
  else if (digitalRead (3) == HIGH)
  {
    if (lastStateIn == 0 && lastStateOut == 0)
    {
      if (fullCrossing (3, 2, 1000))
      {
        Serial.print ("0");
        delay(250);
      }
    }
    lastStateIn = 0;
    lastStateOut = 1;
  }    
  else
  {
    lastStateIn = 0;
    lastStateOut = 0;
  }
}
