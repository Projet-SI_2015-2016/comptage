import sqlite3

class Database():
    def __init__(self, dbfile):
        self.dbfile = dbfile
        self.conn = None
        self.c = None

    def connect_to_db(self):
        self.conn = sqlite3.connect(self.dbfile)
        self.c = self.conn.cursor()

    def create_table(self,name):
        try:
            self.c.execute("CREATE TABLE {} (time TIME, direction TEXT)".format(name))
        except sqlite3.OperationalError:
            return
            
    def insert_content(self,name,heure,direction):
        # the time must be as an int ( 19:30:00 --> 193000 )
        self.c.execute("INSERT INTO {0}(time, direction) VALUES {1}".format(name,(heure,direction)))

    def remove(self,name):
        self.c.execute("DROP TABLE {}".format(name))
    
    def get_all_content(self, table_name):
        try:
            cur = self.c.execute("SELECT * FROM {}".format(table_name))
            user1=self.c.fetchall()
            for row in user1:
                print("{0} - {1}".format(row[0],row[1]))          
        except sqlite3.OperationalError:
            print("La table \'{}' n\'existe pas.".format(table_name))
            
    def get_content(self,table_name):
        # get_content returns the entries nb for every 30 mins
        # as a list named rate
        heures=[]
        a = 0
        for i in range(24):
            heures.append(str(a))
            a += 3000
            heures.append(str(a))
            a += 7000    
        rate=[]
        rates = 0
        a = 0        
        try:
            cur = self.c.execute("SELECT * FROM {}".format(table_name))
            user1 = self.c.fetchall()
            for m in range (48):
                for row in user1:
                    if row[0]>int(heures[m]) and row[0]<int(heures[m+1]):
                        if row[1] == "entree":
                            rates += 1
                rate.append(rates)             
                rates = 0
                                   
        except sqlite3.OperationalError:
            return 0   
        return rate     
                 
    def disconnect_from_db(self):
        self.conn.commit()
        self.conn.close()

